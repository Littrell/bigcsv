module gitlab.com/Littrell/bigcsv

go 1.14

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/google/uuid v1.2.0
	github.com/lucasjones/reggen v0.0.0-20200904144131-37ba4fa293bb
	github.com/manifoldco/promptui v0.8.0
	golang.org/x/text v0.3.6 // indirect
)

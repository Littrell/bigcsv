package main

import (
	"errors"
	"math/rand"
	"time"

	"github.com/Pallinder/go-randomdata"
	"github.com/google/uuid"
	"github.com/lucasjones/reggen"
)

// TODO there's a probably a better way to represent this data

type Color struct {
	Index string
	Name  string
	Hex   string
	R     int
	G     int
	B     int
}

type Column struct {
	Type     ColumnType
	Metadata ColumnMetadata
	Header   string
}

type ColumnMetadata struct {
	Regex string
}

type ColumnType struct {
	Key      string
	Label    string
	Generate ColumnGenerate
}

type ColumnGenerate func(ColumnMetadata) (string, error)

var ColumnTypes = []ColumnType{
	{
		Key:   "uuid",
		Label: "UUID",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return uuid.New().String(), nil
		},
	},
	{
		Key:   "first_name",
		Label: "First name",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.FirstName(randomdata.RandomGender), nil
		},
	},
	{
		Key:   "last_name",
		Label: "Last name",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.LastName(), nil
		},
	},
	{
		Key:   "silly_name",
		Label: "Silly name",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.SillyName(), nil
		},
	},
	{
		Key:   "email",
		Label: "Email",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.Email(), nil
		},
	},
	{
		Key:   "ip_address",
		Label: "IPV4 Address",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.IpV4Address(), nil
		},
	},
	{
		Key:   "city",
		Label: "City",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.City(), nil
		},
	},
	{
		Key:   "state",
		Label: "State",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.State(randomdata.Large), nil
		},
	},
	{
		Key:   "country",
		Label: "Country",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.Country(randomdata.FullCountry), nil
		},
	},
	{
		Key:   "currency",
		Label: "Currency",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.Currency(), nil
		},
	},
	{
		Key:   "phone",
		Label: "Phone number",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return randomdata.PhoneNumber(), nil
		},
	},
	{
		Key:   "color",
		Label: "Color",
		Generate: func(metadata ColumnMetadata) (string, error) {
			return Colors[rand.Intn(len(Colors))].Name, nil
		},
	},
	{
		Key:   "date",
		Label: "Date",
		Generate: func(metadata ColumnMetadata) (string, error) {
			randomEpoch := rand.Int63n(time.Now().Unix())
			randomTime := time.Unix(randomEpoch, 0)
			return randomTime.Format(time.RFC3339), nil
		},
	},
	{
		Key:   "boolean",
		Label: "Boolean",
		Generate: func(metadata ColumnMetadata) (string, error) {
			if rand.Int()%2 == 0 {
				return "true", nil
			} else {
				return "false", nil
			}
		},
	},
	{
		Key:   "regex",
		Label: "Regular Expression",
		Generate: func(metadata ColumnMetadata) (string, error) {
			generatedString, err := reggen.Generate(metadata.Regex, 10)
			if err != nil {
				return "", errors.New("there was an issue with the provided regular expression")
			}
			return generatedString, nil
		},
	},
}

/////////////////////
// BASIC TEMPLATES //
/////////////////////

type Template struct {
	Key   string
	Label string
}
# bigcsv

A Golang CLI tool for generating CSVs

NOTE - This is still a work in progress. I originally put this together as a convenience tool for my own personal use. Most of the features have been "tacked on" and so the organization is subpar, at best. Long term I would like to make this a _sane_ CLI tool.

## Install

```bash
go install gitlab.com/Littrell/bigcsv@latest
```

Or run using Docker:

```bash
docker build -t bcsv-image .
docker run -it --rm -v "$PWD":/go/src/app --name bcsv-container bcsv-image
```

## Setup for development

### Build

```bash
./build.sh
```

### Run

```bash
./bcsv
```

### Install

```bash
go install
```


package main

import (
	"flag"
	"log"
	"os"
)

// TODO get rid of me kthxbye
var Colors []Color

// TODO create an application context?

func main() {

	wide := flag.Bool("wide", false, "Generate a wide CSV")
	missingData := flag.Bool("missing-data", false, "Generate a CSV with random missing data points")
	flag.Parse()

	if *missingData {
		log.Println("missing data has not been implemented yet")
	}

	// Generate a wide CSV with pre-determined columns
	// There may be a better long term solution but for
	// my needs this works fine.
	if *wide {
		rows := PromptRows()
		filename := PromptFilename()
		generate_wide(rows, filename)
		os.Exit(0)
	}

	// Get colors and set globally
	colors, err := GetColors()
	if err != nil {
		log.Fatal(err)
	}
	Colors = colors

	columns := []Column{}

	for {
		column := Column{}

		columnType, err := PromptColumnType()
		if err != nil {
			log.Fatal(err)
		}

		columnHeader, err := PromptColumnHeader()
		if err != nil {
			log.Fatal(err)
		}

		if columnHeader == "" {
			columnHeader = columnType.Label
		}

		if columnType.Key == "regex" {
			regex, err := PromptRegex()
			if err != nil {
				log.Fatal(err)
			}
			column.Metadata.Regex = regex
		}

		column.Type = columnType
		column.Header = columnHeader

		columns = append(columns, column)

		if !PromptTrueFalse("Add another column?") {
			break
		}
	}

	rows := PromptRows()

	filename := PromptFilename()

	generate(rows, columns, filename)
}

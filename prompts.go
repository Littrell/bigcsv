package main

import (
	"errors"
	"log"
	"strconv"

	"github.com/manifoldco/promptui"
)

func PromptColumnType() (ColumnType, error) {
	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}?",
		Active:   "\U00002192 {{ .Label }}",
		Inactive: "{{ .Label }}",
		Selected: "\U00002192 {{ .Label }}",
	}

	prompt := promptui.Select{
		Label:     "What type of column?",
		Items:     ColumnTypes,
		Templates: templates,
	}

	i, _, err := prompt.Run()
	if err != nil {
		return ColumnType{}, err
	}

	return ColumnTypes[i], nil
}

func PromptFilename() string {
	prompt := promptui.Prompt{
		Label: "What is the name of this file? .csv will be appended. (default: generated.csv)",
	}

	result, err := prompt.Run()
	if err != nil {
		log.Println(err)
	}

	if result == "" {
		result = "generated"
	}

	result = result + ".csv"

	return result
}

func PromptColumnHeader() (string, error) {
	prompt := promptui.Prompt{
		Label: "What is the name of the column?",
	}

	result, err := prompt.Run()
	if err != nil {
		return "", err
	}

	return result, nil
}

func PromptRows() int {
	validate := func(input string) error {
		_, err := strconv.Atoi(input)
		if err != nil {
			return errors.New("Invalid number")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "How many rows would you like to generate?",
		Validate: validate,
	}

	result, err := prompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	resultInt, err := strconv.Atoi(result)
	if err != nil {
		log.Fatal(err)
	}

	return resultInt
}

func PromptTrueFalse(label string) bool {
	prompt := promptui.Select{
		Label: label,
		Items: []string{"yes", "no"},
	}

	_, yesNo, err := prompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	if yesNo == "yes" {
		return true
	} else {
		return false
	}
}

func PromptRegex() (string, error) {
	prompt := promptui.Prompt{
		Label: "Regex?",
	}

	result, err := prompt.Run()
	if err != nil {
		return "", err
	}

	if result == "" {
		return "", errors.New("Regex cannot be empty")
	}

	return result, nil
}

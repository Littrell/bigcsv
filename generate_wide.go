package main

import (
	"fmt"
	"log"
)

var WideTemplate = []Template{
	{Key: "uuid", Label: "uuid"},
	{Key: "first_name", Label: "First name"},
	{Key: "first_name", Label: "Middle name"},
	{Key: "last_name", Label: "Last name"},
	{Key: "silly_name", Label: "Nickname"},
	{Key: "email", Label: "Personal email"},
	{Key: "email", Label: "Secondary email"},
	{Key: "email", Label: "Work email"},
	{Key: "phone", Label: "Personal phone #"},
	{Key: "phone", Label: "Fax #"},
	{Key: "phone", Label: "Work phone #"},
	{Key: "ip_address", Label: "Laptop IP Address"},
	{Key: "ip_address", Label: "Desktop IP Address"},
	{Key: "ip_address", Label: "Phone IP Address"},
	{Key: "ip_address", Label: "Smart TV IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 1 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 2 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 3 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 4 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 5 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 6 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 7 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 8 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 9 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 10 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 11 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 12 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 13 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 14 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 15 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 16 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 17 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 18 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 19 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 20 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 21 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 22 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 23 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 24 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 25 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 26 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 27 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 28 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 29 IP Address"},
	{Key: "ip_address", Label: "Unknown IOT Device 30 IP Address"},
	{Key: "city", Label: "City"},
	{Key: "state", Label: "State"},
	{Key: "country", Label: "Country"},
	{Key: "date", Label: "Birthday"},
	{Key: "date", Label: "Updated"},
	{Key: "date", Label: "Created"},

}

func generate_wide(rows int, filename string) {
	fmt.Println("Generating...")

	columns := []Column{}
	// Convert WideTemplate to ColumnType
	for _, columnTemplate := range WideTemplate {
		columnType, err := GetColumnTypeByKey(columnTemplate.Key)
		if err != nil {
			log.Fatal(err)
		}
		column := Column{
			Type: columnType,
			Header: columnTemplate.Label,
		}
		columns = append(columns, column)
	}

	generate(rows, columns, filename)
}
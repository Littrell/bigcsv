package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

func generate(rows int, columns []Column, filename string) {
	fmt.Println("Generating...")

	data := [][]string{}
	headers := []string{}
	for _, column := range columns {
		headers = append(headers, column.Header)
	}
	data = append(data, headers)

	for i := 0; i < rows; i++ {
		row := []string{}
		for _, column := range columns {
			cell, err := column.Type.Generate(column.Metadata)
			if err != nil {
				log.Fatal(err)
			}
			row = append(row, cell)
		}
		data = append(data, row)
	}

	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			log.Fatal(err)
		}
	}
}

package main

import (
	"encoding/csv"
	"errors"
	"log"
	"os"
	"strconv"
)

func GetColors() ([]Color, error) {
	// Load up some initial data
	colorsFile, err := os.Open("data/colors.csv")
	if err != nil {
		log.Println("There was an error opening the colors file")
		return nil, err
	}
	defer colorsFile.Close()

	colorsReader := csv.NewReader(colorsFile)
	records, err := colorsReader.ReadAll()
	if err != nil {
		log.Println("There was an issue reading the colors.csv file")
		return nil, err
	}

	var colors []Color
	for _, record := range records {
		R, err := strconv.Atoi(record[3])
		if err != nil {
			return nil, err
		}

		G, err := strconv.Atoi(record[4])
		if err != nil {
			return nil, err
		}

		B, err := strconv.Atoi(record[5])
		if err != nil {
			return nil, err
		}

		color := Color{
			Index: record[0],
			Name:  record[1],
			Hex:   record[2],
			R:     R,
			G:     G,
			B:     B,
		}

		colors = append(colors, color)
	}

	return colors, nil
}

func GetColumnTypeByKey(key string) (ColumnType, error) {
	for _, columnType := range ColumnTypes {
		if key == columnType.Key {
			return columnType, nil
		}
	}
	return ColumnType{}, errors.New("column type not found")
}